@extends('layouts.app')

@section('content')
    <div class="w-100 d-flex justify-content-center align-items-center">
        <div class="text-center w-25">
            <h1 class="pt-5 pb-2">Hi, there!</h1>

            <form action="{{ route('task.store') }}" method="POST">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" name="task" class="form-control" placeholder="What do you want to do next?">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" title="Add Task">
                            <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </form>

            <h4 class="pt-5 pb-3">My Tasks:</h4>
            <div class="w-100">
                @forelse($tasks as $task)
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="p-3">
                            @if(!$task->isCompleted)
                                <svg width="3em" height="1.5em" viewBox="0 0 16 16" class="bi bi-arrow-right-short" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                                </svg>
                                {{ $task->task }}
                            @else
                                <svg width="3em" height="1.5em" viewBox="0 0 16 16" class="bi bi-check-square-fill" fill="#38c172" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm10.03 4.97a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                                </svg>
                                <strike>{{ $task->task }}</strike>
                            @endif
                        </div>

                        <div class="d-flex align-items-center">
                            @if($task->isCompleted)
                                <form action="{{ route('task.update', $task->id) }}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <input type="text" name="isCompleted" value="0" hidden>
                                    <button class="m-1 btn btn-warning" title="Mark as Not Completed">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-skip-backward" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M.5 3.5A.5.5 0 0 1 1 4v3.248l6.267-3.636c.52-.302 1.233.043 1.233.696v2.94l6.267-3.636c.52-.302 1.233.043 1.233.696v7.384c0 .653-.713.998-1.233.696L8.5 8.752v2.94c0 .653-.713.998-1.233.696L1 8.752V12a.5.5 0 0 1-1 0V4a.5.5 0 0 1 .5-.5zm7 1.133L1.696 8 7.5 11.367V4.633zm7.5 0L9.196 8 15 11.367V4.633z"/>
                                        </svg>
                                    </button>
                                </form>
                            @else
                                <form action="{{ route('task.update', $task->id) }}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <input type="text" name="isCompleted" value="1" hidden>
                                    <button class="m-1 btn btn-success" title="Mark as Completed">
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
                                        </svg>
                                    </button>
                                </form>
                            @endif

                            <a class="m-1 btn btn-outline-info" title="Edit Task" href="{{ route('task.edit', $task->id) }}">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                </svg>
                            </a>

                            <form action="{{ route('task.destroy', $task->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="m-1 btn btn-outline-danger" title="Delete Task">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-dash-circle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM4.5 7.5a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7z"/>
                                    </svg>
                                </button>
                            </form>
                        </div>
                    </div>
                @empty
                    <p class="p-4">No tasks found. Add something to your list now!</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
